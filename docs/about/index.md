# About me

![Picture of me!](http://fabacademy.org/2019/labs/kamplintfort/students/hakan-zayin/media/about.JPG)

Hi! My Name is Hakan Zayin, I am studying computer science at the Hochschule Rhein-Waal in Kamp-Lintfort. 

Visit this website to see my work!

## My background

I was born in a nice city called Duisburg, but live in Moers eversince.
Im interested in traveling, playing videogames and any kind of technology that I can get into my hands. I would say that I am very creative when it comes to looking for an idea. From very crazy und unproducable to useful and easy to make

## Previous work

Previously I have finished the FabAcademy 2019, building a electric gokart as my final project.

### Electric Gokart

My final project for the fabacademy 2019

![Gokart](http://fabacademy.org/2019/labs/kamplintfort/students/hakan-zayin/media/finalkart.jpg)


