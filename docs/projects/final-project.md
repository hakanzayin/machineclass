# Final Project

Welcome to the documentation of my final project. If you have red my documentation of the last few weeks it may occur that some of the parts of this documentation repeats the steps that I described in the last assignments.

## Research

I looked for different ways of 3d printing for example:

- Wasp
![](../images/final/f01.jpg)

- CoreXY
![](../images/final/f02.png)

- BigFDM
![](../images/final/f03.jpg)

I decided to use the same technique as the bigFDM. What I really liked about the bigFDM was the fact that it uses linear guides to move the axis. I would not say that a printer that is not using linear guides is a bad printer, but if you think about every big machine for example cnc-machines, they most likely use linear guides. Linear guides will not only give stability to my printing axes, they will also have a very long lifetime.

## Workflow

#Frame
So I started designing my printer, I started off with the frame. I have used 20x20mm aluminium frames. In my opinion they are the best to create a stable frame that also can be modified everytime.

![](../images/final/f04.png)

It was not a big deal to screw the frame together. I had to watch out that every connection was exactly 90 degrees and that the connection angles are not too big.

![](../images/final/f05.jpg)

#Z-Axis
I went on with designing and producing the holding pieces for the z axis.

![](../images/final/f06.png)

I used the lasercutter to cut the parts out of 12mm plexiglas. The settings I used are the following:

- Speed: 7%
- Power: 100%
- Frequency: 30%

I used these parts for the bottom and top of each corner.

![](../images/final/f07.jpg)

To Attach the Threaded rods to the holding pieces and to the pulleys I have used the lathe to reduce the diameter of the rods to 5mm.

![](../images/final/f08.jpg)

The whole construction at the bottom will look like this

![](../images/final/f09.png)

The only thing different at the top is some distanceplates 

![](../images/final/f10.png)

The next parts are pieces to hold the printing bed. These parts are the heart of the Z-Axis because they will move up and down

![](../images/final/f11.png)

I produced that parts from 12mm acryl again and used the same settings

![](../images/final/f12.jpg)

I designed a simple plate for the bed that will fit onto the holding pieces

![](../images/final/f13.png)

I milled the bed out of aluminium, but this can be produced out of every material that will not change his attributes when getting heated up.

![](../images/final/f14.jpg)

# Y-Axis

I needed to mill a piece out of pom first that will hold the dual shaft stepper

![](../images/final/f15.png)

You can check out the settings in my 3d-milling [assignment](https://hakanzayin.gitlab.io/machineclass/assignments/6.%203D%20Milling/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/ukFtVi7ykes" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In the next step I had to use the lathe to reduce the diameter of the steel rods to 5mm so that i can screw the pulleys to the ends.

![](../images/final/f16.png)

To attach the rods to the frame I have 3d-printed holding pieces that will fit a bearing and will hold the rod.

![](../images/week09/0907.jpg)

I attached linear guides to the inner sides of the frame. To connect the stepper to the linear guides I milled two more parts out of pom using the same settings as before

![](../images/final/f17.png)

![](../images/final/f18.png)

I will conect these two parts using a single aluminum frame part.

![](../images/final/f19.png)

Now after putting a pulley to the end of each linear guide I was able to connect the moving part to the motor.

![](../images/final/f20.png)

# X-Axis
For the x-axis I started off by attaching a linear guide to the moving aluminium frame.

![](../images/final/f21.png)

I lasercutted a holder for the printhead, I have used 12mm acryl for thus part because I had to make sure that the head will not wobble. I tried to use a 8mm part but it was mooving to much.

![](../images/final/f22.png)

After that I simply screwed the whole printhead around this holding piece. I must make sure that the heatsink was touching the extruder because it needs cooling.

![](../images/final/f23.jpg)

To move the printhead, I lasercutted a holdingplate for the smaller nema14 stepper that I attached to the frame part.

![](../images/final/f24.png)

the pulley which I screwed onto the shaft of this stepper will move the printhead.

![](../images/final/f25.jpg)

On the other side I have milled a holder for the second pulley that will just turn by putting it loose onto a screw.

![](../images/final/f26.jpg)

In order to place the endstops I 3d-printed holding pieces. These can be mounted to the frame very flexible for all the three axis.

![](../images/final/f27.jpg)



# Electronics
To control the printer I have used the ramps 1.4 in combination to a Arduino Mega.

![](../images/final/ramps.jpg)

In order to move the stepper I put stepperdriver onto the ramps and attached every electronic part f.e. Stepper, Nozzleheater, Bedheater, Endstops onto the board. The Ramps kit is more ore less self explenatory, to find out more about the software you can check my [electronics assignment](https://hakanzayin.gitlab.io/machineclass/assignments/week10/)

## Final result

![](../images/final/f28.jpeg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LmcYlXMAdDg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Bill of Material
Hit the link up for a list of all the materials that I had to order to build this printer.

[Printer BOM](https://docs.google.com/spreadsheets/d/1Yyx3AkUVm_zr3Ipvcgg4cg7EemY9cZZsaEYaiSSnfUg/edit?usp=sharing)

## 3D-Models
All holding piece models can be found in my design. I have milled the parts for the x and y axis out of pom.

[Click here to download](../../data/HakanPrinter.f3d)
