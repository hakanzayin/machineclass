 # 7. Electronics Production

This week I startde to produce the first electronics for my final project

## Research

Since I am builing a 3d-printer I needed to produce a circuit board that will be able to move and power the different motors of my printer. I have come to the conclusion that the satshakit 128 which uses the atmega 128p microprocessor.

All the information of the board I will produce can be found in the documentation of the [satshakit-128](https://github.com/satshakit/satshakit-128) on gitHub.

I have used this internal cut
![](../images/week06/0602.png)

and have used that outside cut

![](../images/week06/0601.png)

Attention! I would not recommend using the png from my website please follow [this link](https://github.com/satshakit/satshakit-128) to get the original png's from the documentation.

I have created the gcode for the mill using the [fabmodules website](http://fabmodules.org).

If you need a introduction into the fabmodules app and also need some help with the milling settings you can check out my [fabacademy website!]
(http://fabacademy.org/2019/labs/kamplintfort/students/hakan-zayin/week5.html)

Finally after milling and soldering my finished board looked like this 

![](../images/week06/0603.JPG)

